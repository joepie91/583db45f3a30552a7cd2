var Promise = require("es6-promise").Promise;

module.exports = function(delay){
	return function(value) {
		return new Promise(function(resolve, reject){
			setTimeout(function(){
				resolve(value);
			}, delay)
		});
	}
}