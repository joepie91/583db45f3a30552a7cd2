var delay = require("./delay");

// ...
.then(function(value){
	return doAnotherThing();
})
.then(delay(5000))
.then(function(value){
	return doThingAfterFiveSeconds(value);
})
// ...